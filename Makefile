develop:
	pip install -e .[dev,data]

test:
	pytest -vv -x --ff

lint:
	black --check --diff zonages test
	flake8 zonages test

# Le chargement des données est dans un Makefile dédié
update_data:
	$(MAKE) -C zonages/data

serve:
	uvicorn zonages:app --reload

.PHONY: develop test lint update_data serve
