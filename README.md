# API de zonages

Prend une latitude, une longitude et un type de zonage en paramètre, et si le
point est dans un zonage, retourne ses propriétés.

NB: Les points sont donnés dans le système de coordonnées géographique **EPSG: 4326**.


## Installation locale

### Dépendances

- PostgreSQL
- PostGIS
- `shp2pgsql` (fourni avec PostGIS)
- `ogr2ogr` (dans le paquet `gdal-bin` sous Debian/Ubuntu)
- Python 3.9
- 7zip (dans le paquet `p7zip-full` sous Debian/Ubuntu, ou `p7zip` sur mac avec Homebrew)

### Préparation

- créer une base de données `zonages` : `createdb zonages; psql --dbname zonages -c "CREATE EXTENSION IF NOT EXISTS postgis"`
- créer une base de données de test`test_zonages` : `createdb test_zonages;  psql --dbname test_zonages -c "CREATE EXTENSION IF NOT EXISTS postgis"`
- installer les dépendances python: `make develop`

## Mise à jour des données

```bash
export PGDATABASE=zonages
export PGHOST=localhost
make update_data
```

NB: la donnée iris étant très lourde à télécharger et à transformer en fichier sql, il est possible de la télécharger directement depuis data.gouv avec la commande `make iris.download.dgv`

## Lancer l’API en local

- `uvicorn zonages:app --reload`

Servie par défaut sur `http://127.0.0.1:8000`

## Alternative : Docker Compose

Préparer les données Iris localement (optionnel) :
- `sudo apt install postgis` (Linux) ou `brew install postgis` (macOS)
- `make iris.download.source iris.sql`

Lancer l’API (servie sur `http://127.0.0.1:8000`) :
- `docker-compose up`

Mettre à jour les données via un conteneur :
- `docker-compose run update_data`

## Routes

- `/` : interface utilisateur
- `/api/` : API
- `/doc/` : documentation

## Exemples

```
$ curl "http://127.0.0.1:8000/api/?lat=48.9557&lon=2.2925&zonages=qpv" | jq .
{
  "qpv": {
    "code": "QP093030",
    "nom": "Orgemont",
    "commune": "Épinay-sur-Seine",
    "source": {
      "nom": "ANCT",
      "url": "https://www.data.gouv.fr/fr/datasets/quartiers-prioritaires-de-la-politique-de-la-ville-qpv/",
      "licence": "Licence Ouverte"
    }
  }
}
```

```
$ curl "http://127.0.0.1:8000/api/?lat=44.333328&lon=1.21667&zonages=tous" | jq .
{
  "iris": {
    "code": "462010000",
    "nom": "Montcuq-en-Quercy-Blanc",
    "type": {
      "code": "Z",
      "description": "commune non découpée en IRIS"
    },
    "commune": {
      "code_insee": "46201",
      "nom": "Montcuq-en-Quercy-Blanc",
      "cog": 2022
    },
    "source": {
      "nom": "IGN",
      "url": "https://www.data.gouv.fr/fr/datasets/contours-iris/#community-resources",
      "licence": "Licence Ouverte"
    }
  },
  "qpv": {
    "code": null,
    "nom": null,
    "commune": null,
    "source": {
      "nom": "ANCT",
      "url": "https://www.data.gouv.fr/fr/datasets/quartiers-prioritaires-de-la-politique-de-la-ville-qpv/",
      "licence": "Licence Ouverte"
    }
  },
  "zrr": {
    "commune": {
      "code_insee": "46201",
      "nom": "Montcuq-en-Quercy-Blanc",
      "cog": 2017
    },
    "classement": {
      "code": "C",
      "description": "Classée en ZRR"
    },
    "detail": {
      "code": "C",
      "description": "Classée en ZRR"
    },
    "source": {
      "nom": "ANCT",
      "url": "https://www.data.gouv.fr/fr/datasets/zones-de-revitalisation-rurale-zrr/",
      "licence": "Licence Ouverte"
    }
  },
  "densite": {
    "commune": {
      "code_insee": "46201",
      "cog": 2022,
      "nom": "Montcuq-en-Quercy-Blanc"
    },
    "densite": {
      "degre": 6,
      "description": "Rural à habitat dispersé"
    },
    "source": {
      "nom": "INSEE",
      "url": "https://www.insee.fr/fr/information/6439600",
      "licence": null
    }
  },
  "afr": {
    "code": null,
    "nom": null,
    "echelon": null,
    "source": {
      "nom": "ANCT",
      "url": "https://www.data.gouv.fr/fr/datasets/zones-daide-a-finalite-regionale-afr/",
      "licence": "Licence Ouverte"
    }
  }
}
```