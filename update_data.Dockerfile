FROM debian:11.4-slim

RUN apt-get update && \
    apt-get install --yes python3-pip make p7zip-full postgis unzip wget && \
    rm -rf /var/lib/apt/lists/*

COPY ./Makefile Makefile
COPY ./zonages/data zonages/data

ENV PIP_ROOT_USER_ACTION=ignore
ENV PYTHONUNBUFFERED=1
RUN pip install -U pip
RUN pip install --no-cache-dir frictionless[excel]==5.0.0b9

CMD ["make", "update_data"]
