FROM python:3.9

WORKDIR /code

ENV PIP_ROOT_USER_ACTION=ignore

ENV PYTHONUNBUFFERED=1

COPY ./MANIFEST.in /code/MANIFEST.in
COPY ./setup.py /code/setup.py
COPY ./setup.cfg /code/setup.cfg
COPY ./static /code/static
COPY ./zonages /code/zonages

RUN pip install pip==22.1.2
RUN pip install --no-cache-dir --upgrade -e /code

CMD ["uvicorn", "zonages:app", "--host", "0.0.0.0", "--port", "80"]
