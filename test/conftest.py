import os

import pytest
import pytest_asyncio

from zonages.config import settings
from zonages.db import create_pool

pytestmark = pytest.mark.asyncio


@pytest.fixture(scope="session", autouse=True)
def override_settings():
    settings.DBNAME = os.getenv("TEST_DBNAME", "test_zonages")


@pytest_asyncio.fixture()
async def db_conn():
    db_pool = await create_pool()
    async with db_pool.acquire() as conn:
        yield conn
