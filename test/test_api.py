import pytest

from fastapi.testclient import TestClient

from zonages import app


@pytest.fixture()
def client():
    # On utilise un bloc "with" pour que les événements startup et shutdown de l’app
    # soient appelés (cf. https://fastapi.tiangolo.com/advanced/testing-events/)
    with TestClient(app) as client:
        yield client


def test_get_zonage_without_parameters(client):
    response = client.get("/api/")
    assert response.status_code == 422


def test_get_zonage_without_zonages(client):
    response = client.get("/api/?lat=1&lon=2")
    assert response.status_code == 422
    assert response.json() == {
        "detail": "Missing `zonages` parameter."
        " Valid values are: ['iris', 'qpv', 'zrr', 'densite', 'afr', 'tous']"
    }


def test_get_zonage_with_unknown_zonages(client):
    response = client.get("/api/?lat=1&lon=2&zonages=unknown")
    assert response.status_code == 422
    assert response.json() == {"detail": "Unknown `zonages` values: ['unknown']"}


def test_get_zonage_with_all_zonages(client, monkeypatch):
    async def mock_lookup(self, lon, lat):
        return {}

    monkeypatch.setattr("zonages.densite.Densité.lookup", mock_lookup)
    monkeypatch.setattr("zonages.iris.IRIS.lookup", mock_lookup)
    monkeypatch.setattr("zonages.qpv.QPV.lookup", mock_lookup)
    monkeypatch.setattr("zonages.zrr.ZRR.lookup", mock_lookup)
    monkeypatch.setattr("zonages.afr.AFR.lookup", mock_lookup)

    response = client.get("/api/?lat=1&lon=2&zonages=tous")
    assert response.status_code == 200
    assert set(response.json().keys()) == {"iris", "qpv", "zrr", "densite", "afr"}


def test_get_zonage(client, monkeypatch):
    async def mock_qpv_lookup(self, lon, lat):
        return {
            "code": "QP093030",
            "nom": "Orgemont",
            "commune": "Épinay-sur-Seine",
        }

    monkeypatch.setattr("zonages.qpv.QPV.lookup", mock_qpv_lookup)
    response = client.get(
        "/api/", params={"lat": 12.3, "lon": 4.56, "zonages": ["qpv"]}
    )
    assert response.status_code == 200
    assert response.json() == {
        "qpv": {
            "code": "QP093030",
            "nom": "Orgemont",
            "commune": "Épinay-sur-Seine",
        }
    }


def test_get_zonage_with_point_outside_qpv(client, monkeypatch):
    async def mock_qpv_lookup(self, lon, lat):
        return None

    monkeypatch.setattr("zonages.qpv.QPV.lookup", mock_qpv_lookup)
    response = client.get(
        "/api/", params={"lat": 12.3, "lon": 4.56, "zonages": ["qpv"]}
    )
    assert response.status_code == 200
    assert response.json() == {"qpv": None}
