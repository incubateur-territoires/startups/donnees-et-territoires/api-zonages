// reference
const input = document.getElementById("input");

input.addEventListener("keyup", (e) => {
  if (input === document.activeElement) {
    fetch(`https://api-adresse.data.gouv.fr/search/?q=${input.value}`)
      .then((response) => response.json())
      .then((data) => {
        document.querySelector(".list").textContent = "";
        data.features.forEach((feat) => {
          const listItem = document.createElement("li");
          listItem.classList.add("list-items");
          listItem.style.cursor = "pointer";
          listItem.onclick = function () {
            selectAddress(feat);
          };
          listItem.textContent = feat.properties.label;
          document.querySelector(".list").appendChild(listItem);
        });
        if (data.features.length === 0) {
          const listItem = document.createElement("li");
          listItem.classList.add("list-items");
          listItem.textContent = "Pas de résultats";
          document.querySelector(".list").appendChild(listItem);
        }
      });
  }
});

function selectAddress(value) {
  document.querySelector(".list").textContent = "";
  input.value = value.properties.label;
  document.querySelector("#resultTitle").innerHTML = value.properties.label;
  const coords = value.geometry.coordinates;
  fetch(
    `/api/?lat=${coords[1]}&lon=${coords[0]}&zonages=tous`,
    {
      mode: "cors",
      headers: {
        "Access-Control-Allow-Origin": "*",
      },
    }
  )
    .then((data) => data.json())
    .then((j) => {
      const irisTypeDescription = getIrisTypeDescription(j.iris.type);
      const cards = [];
      cards.push(
        generateCard(
          "IRIS",
          j.iris.code
            ? [
                ["Code", j.iris.code],
                ["Nom", j.iris.nom],
                ["Type", irisTypeDescription],
                ["Commune", j.iris.commune.nom],
                ["Code INSEE", j.iris.commune.code_insee],
                ["Version du COG", j.iris.commune.cog],
              ]
            : [["Cette zone n’est pas dans un IRIS."]],
          j.iris && j.iris.source
        )
      );
      cards.push(
        generateCard(
          "QPV",
          j.qpv.code
            ? [
                ["Code", j.qpv.code],
                ["Nom", j.qpv.nom],
                ["Commune", j.qpv.commune],
              ]
            : [["Cette zone n’est pas un QPV."]],
          j.qpv && j.qpv.source
        )
      );
      cards.push(
        generateCard(
          "ZRR",
          j.zrr.commune.nom
            ? [
                ["Commune", j.zrr.commune.nom],
                ["Code INSEE", j.zrr.commune.code_insee],
                ["Version du COG", j.zrr.commune.cog],
                ["Classement", j.zrr.classement.description],
                ["Détail", j.zrr.detail.description],
              ]
            : [["Commune non identifiée."]],
          j.zrr && j.zrr.source
        )
      );
      cards.push(
        generateCard(
          "Densité",
          j.densite.commune.nom
            ? [
                ["Commune", j.densite.commune.nom],
                ["Code INSEE", j.densite.commune.code_insee],
                ["Version du COG", j.densite.commune.cog],
                [
                  "Densité",
                  `${j.densite.densite.description} (${j.densite.densite.degre})`,
                ],
              ]
            : [["Commune non identifiée."]],
          j.densite && j.densite.source
        )
      );
      cards.push(
        generateCard(
          "AFR",
          j.afr.code
            ? [
                ["Cette adresse est en zone AFR."],
                ["Code", j.afr.code],
                ["Nom", j.afr.nom],
                ["Échelon", j.afr.echelon],
              ]
            : [["Cette adresse n’est pas en zone AFR."]],
          j.afr && j.afr.source
        )
      );
      document.querySelector("#results").innerHTML = cards.join("\n");
    });
}

const getLiForAttribute = (attr) => {
  if (attr.length > 1) {
    return `<li><span class="title">${attr[0]} :</span> ${attr[1]}</li>`;
  } else if (attr.length === 1) {
    return `<li>${attr[0]}</li>`;
  }
};

const generateCard = (title, attributes, source) => {
  const licence = source && source.licence ? `&nbsp;(<i>${source.licence}</i>)` : ""
  let detail = source
    ? `
    <div class="fr-card__end">
      <p class="fr-card__detail">
        Source&nbsp;:&nbsp;<a href="${source.url}">${source.nom}</a>
        ${licence}
      </p>
    </div>`
    : "";
  return `
      <div class="fr-card fr-card--horizontal">
          <div class="fr-card__body">
              <div class="fr-card__content">
                  <p class="fr-card__desc">
                      <ul>
                          ${attributes.map(getLiForAttribute).join("")}
                      </ul>
                  </p>
                  ${detail}
              </div>
          </div>
          <div class="fr-card__header center-content">
              <h3 class="fr-card__title">${title}</h3>
          </div>
      </div>
      `;
};

const getIrisTypeDescription = (type) => {
  return `${type.code} (${type.description})`
};
