## 2022.12.13

- corrige la densité pour Paris, Lyon et Marseille
- ajout de statistiques d’utilisation avec Matomo

## 2022.11.22

- ajoute les zones de revitalisation rurale (ZRR)
- ajoute la grille communale de densité de l’INSEE
- ajoute un paramètre tour demander tous les zonages
- uniformise la structure des réponses de l’API
- expose l’interface utilisateur à la racine et la doc sous /doc/

## 2022.10.06

- ajoute une interface utilisateur sur /ui/
- utilise un pool de connexions à la base de données

## 2022.08.26

- version initiale
