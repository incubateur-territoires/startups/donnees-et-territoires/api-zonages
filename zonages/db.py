import asyncio

import asyncpg
from postgis.asyncpg import register as register_postgis_extension

from .config import settings


async def create_pool(min_size=1, max_size=1):
    """
    Crée un pool de connexions à la base de données.
    """
    return await asyncpg.create_pool(
        min_size=min_size,
        max_size=max_size,
        database=settings.DBNAME,
        host=settings.DBHOST,
        port=settings.DBPORT,
        user=settings.DBUSER,
        password=settings.DBPASS,
        setup=register_postgis_extension,
    )


async def shutdown_pool(pool):
    """
    Ferme les connexions à la base de données.
    """
    try:
        await asyncio.wait_for(pool.close(), timeout=5.0)
    except asyncio.TimeoutError:
        await pool.terminate()
