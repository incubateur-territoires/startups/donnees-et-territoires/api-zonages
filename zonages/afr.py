from .source import Source
from .zonage import Zonage


class AFR(Zonage):
    """
    Aides à finalité régionale (AFR)
    """

    SOURCE = Source(
        nom="ANCT",
        url="https://www.data.gouv.fr/fr/datasets/zones-daide-a-finalite-regionale-afr/",  # noqa
        licence="Licence Ouverte",
    )

    SQL_QUERY = """
        SELECT insee_geo, lib_geo, echelon_geo FROM afr
            WHERE ST_Contains(geom, $1)
        """

    def format_row(self, row: dict) -> dict:
        row = {
            "code": row.get("insee_geo"),
            "nom": row.get("lib_geo"),
            "echelon": row.get("echelon_geo"),
        }
        return super().format_row(row)
