from pydantic import BaseSettings


class Settings(BaseSettings):
    DBNAME: str = "zonages"
    DBHOST: str = "localhost"
    DBPORT: int = 5432
    DBUSER: str = None
    DBPASS: str = None


settings = Settings()
