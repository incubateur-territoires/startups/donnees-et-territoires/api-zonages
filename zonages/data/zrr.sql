DROP TABLE IF EXISTS "zrr";

CREATE TABLE "zrr" (
    insee_com VARCHAR(5) NOT NULL,  -- Code Insee (COG 2017)
    lib_com TEXT NOT NULL,          -- Libellé de la commune
    classement VARCHAR(2),          -- Classement en ZRR
    detail VARCHAR(2)               -- Modalités du zonage en ZRR
);
ALTER TABLE "zrr" ADD PRIMARY KEY (insee_com);

\copy "zrr" FROM 'zrr-communes-2017.csv' DELIMITER ',' CSV HEADER;
