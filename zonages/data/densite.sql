DROP TABLE IF EXISTS "densite";

CREATE TABLE "densite" (
    insee_com VARCHAR(5) NOT NULL,
    degre_densite INTEGER
);
ALTER TABLE "densite" ADD PRIMARY KEY (insee_com);

\copy "densite" FROM 'densite.csv' DELIMITER ',' CSV HEADER;
