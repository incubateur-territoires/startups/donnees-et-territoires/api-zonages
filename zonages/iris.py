from .source import Source
from .zonage import Zonage


class IRIS(Zonage):
    """
    Maillage IRIS de l’INSEE

    cf. https://www.insee.fr/fr/metadonnees/definition/c1523
    """

    SOURCE = Source(
        nom="IGN",
        url="https://www.data.gouv.fr/fr/datasets/contours-iris/#community-resources",  # noqa
        licence="Licence Ouverte",
    )

    SQL_QUERY = """
        SELECT insee_com, nom_com, iris, code_iris, nom_iris, typ_iris FROM iris
            WHERE ST_Contains(geom, $1) LIMIT 1
        """

    def format_row(self, row: dict) -> dict:
        row = {
            "code": row.get("code_iris"),
            "nom": row.get("nom_iris"),
            "type": self._expand(row.get("typ_iris"), self.TYPES),
            "commune": {
                "code_insee": row.get("insee_com"),
                "nom": row.get("nom_com"),
                "cog": 2022,
            },
        }
        return super().format_row(row)

    def _expand(self, code, descriptions):
        return {
            "code": code,
            "description": descriptions.get(code, code),
        }

    TYPES = {
        "A": "activité",
        "D": "divers",
        "H": "habitat",
        "Z": "commune non découpée en IRIS",
    }
