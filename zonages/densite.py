from .source import Source
from .zonage import Zonage


DEGRES_DENSITE = {
    1: "Grands centres urbains",
    2: "Centres urbains intermédiaires",
    3: "Petites villes",
    4: "Ceintures urbaines",
    5: "Bourgs ruraux",
    6: "Rural à habitat dispersé",
    7: "Rural à habitat très dispersé",
}


class Densité(Zonage):
    """
    Grille communale de densité à 7 niveaux
    """

    SOURCE = Source(
        nom="INSEE",
        url="https://www.insee.fr/fr/information/6439600",
    )

    # « Le fichier téléchargeable propose la composition communale de la grille de
    # densité détaillée dans la géographie communale en vigueur au 1er janvier 2022 »
    VERSION_COG = 2022

    SQL_QUERY = """
        SELECT iris.insee_com, iris.nom_com, densite.degre_densite
          FROM iris
          JOIN densite ON densite.insee_com=CASE
            WHEN iris.insee_com::INTEGER >= 75101 AND iris.insee_com::INTEGER <= 75120
              THEN '75056'
            WHEN iris.insee_com::INTEGER >= 69381 AND iris.insee_com::INTEGER <= 69389
              THEN '69123'
            WHEN iris.insee_com::INTEGER >= 13201 AND iris.insee_com::INTEGER <= 13216
              THEN '13055'
            ELSE iris.insee_com
          END
          WHERE ST_Contains(iris.geom, $1)
          LIMIT 1
        """

    def format_row(self, row: dict) -> dict:
        row = {
            "commune": {
                "code_insee": row.get("insee_com"),
                "cog": self.VERSION_COG,
                "nom": row.get("nom_com"),
            },
            "densite": self._expand_degre(row.get("degre_densite")),
        }
        return super().format_row(row)

    def _expand_degre(self, degre):
        return {
            "degre": degre,
            "description": DEGRES_DENSITE.get(degre),
        }
