from abc import ABC
from typing import Optional

from asyncpg import Connection
from postgis import Point


class Zonage(ABC):
    def __init__(self, connection: Connection):
        self.connection = connection

    SQL_QUERY: str

    async def lookup(self, lon: float, lat: float) -> Optional[dict]:
        row = await self.connection.fetchrow(
            self.SQL_QUERY,
            Point(lon, lat, srid=4326),
        )
        if row is None:
            row = {}
        return self.format_row(dict(row))

    def format_row(self, row: dict) -> dict:
        if hasattr(self, "SOURCE"):
            row["source"] = getattr(self, "SOURCE")._asdict()
        return row
