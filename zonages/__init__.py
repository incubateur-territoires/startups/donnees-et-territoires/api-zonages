from typing import List, Optional

from asyncpg import Pool
from fastapi import FastAPI, Query, HTTPException
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from starlette.status import HTTP_301_MOVED_PERMANENTLY

from . import db

from .iris import IRIS
from .qpv import QPV
from .zrr import ZRR
from .densite import Densité
from .afr import AFR


ZONAGES = {
    "iris": IRIS,
    "qpv": QPV,
    "zrr": ZRR,
    "densite": Densité,
    "afr": AFR,
}

CHOIX_ZONAGES = list(ZONAGES) + ["tous"]

DESCRIPTION = """
L’API de zonages permet de savoir si un point géographique se trouve dans un
certain type de zonage administratif.

Les zonages pris en charge actuellement sont :
- le découpage des communes en [IRIS](https://www.insee.fr/fr/metadonnees/definition/c1523)
- les [quartiers prioritaires de la ville](https://www.cohesion-territoires.gouv.fr/quartiers-de-la-politique-de-la-ville) (QPV)
- les [zones de revitalisation rurale](https://www.observatoire-des-territoires.gouv.fr/kiosque/zonage-les-zones-de-revitalisation-rurale-zrr) (ZRR)
- les [aides à finalité régionale](https://www.collectivites-locales.gouv.fr/cohesion-territoriale/aide-finalite-regionale-afr) (AFR)
- la [grille communale de densité](https://www.insee.fr/fr/information/6439600) de l’INSEE

Contactez [Données et Territoires](mailto:donnees@anct.gouv.fr) pour l’ajout de
nouveaux types de zonages.

## Clause de non-responsabilité

Les données renvoyées par cette API sont seulement **indicatives** et n’ont aucune valeur juridique.

Cette API est mise à disposition par Données et Territoires, startup de l’[ANCT](https://www.cohesion-territoires.gouv.fr),
elle s’appuie sur des jeux de données publiés sous licence libre.

Le code source est libre et [disponible ici](https://gitlab.com/incubateur-territoires/startups/donnees-et-territoires/api-zonages).
"""  # noqa

VERSION = "2022.12.13"

app = FastAPI(
    title="Api de zonages",
    description=DESCRIPTION,
    version=VERSION,
    contact={
        "name": "Données et Territoires",
        "url": "https://donnees.incubateur.anct.gouv.fr",
        "email": "donnees@anct.gouv.fr",
    },
    docs_url="/doc/",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

# Use a global database connection pool
db_pool: Optional[Pool] = None


@app.on_event("startup")
async def startup():
    print("Creating database connection pool...")
    global db_pool
    db_pool = await db.create_pool()
    print("Successfully created database connection pool.")


@app.on_event("shutdown")
async def shutdown():
    print("Closing database connections in pool...")
    await db.shutdown_pool(db_pool)
    print("Successfully closed database connection pool.")


@app.get("/api/")
async def get_zonage(
    lat: float = Query(description="Latitude en EPSG:4326"),
    lon: float = Query(description="Longitude en EPSG:4326"),
    zonages: List = Query(
        default=[],
        description=f"Codes des zonages voulus, parmi: {CHOIX_ZONAGES}",
    ),
):
    """
    Obtenir les zonages associés à des coordonnées géographiques.

    La requête prend en paramètre une latitude (`lat`), une longitude
    (`lon`) et le nom d’un ou plusieurs zonages disponibles (eg. `zonages=qpv`
    ou `zonages=qpv&zonages=iris`), ou la valeur `tous` pour obtenir des
    informations concernant l’ensemble des zonages.
    """

    # Paramètre manquant?
    if not zonages:
        # Raise our own exception so to have a nicer error message.
        raise HTTPException(
            status_code=422,
            detail=f"Missing `zonages` parameter. Valid values are: {CHOIX_ZONAGES}",
        )

    # Paramètres invalides?
    unknown_zonages = set(zonages) - set(CHOIX_ZONAGES)
    if unknown_zonages:
        raise HTTPException(
            status_code=422,
            detail=f"Unknown `zonages` values: {list(unknown_zonages)}",
        )

    # Raccourci pour avoir tous les zonages
    if "tous" in zonages:
        zonages = list(ZONAGES)

    assert db_pool is not None
    async with db_pool.acquire() as connection:
        return {
            zonage: await class_(connection=connection).lookup(lon=lon, lat=lat)
            for zonage, class_ in ZONAGES.items()
            if zonage in zonages
        }


@app.get("/ui/", include_in_schema=False)
async def redirect_old_ui_route():
    return RedirectResponse("/", status_code=HTTP_301_MOVED_PERMANENTLY)


# L’interface web de requêtage est exposée à la racine.
# NB: on enregistre cette route en dernier pour ne pas masquer les autres.
app.mount("/", StaticFiles(directory="static", html=True), name="static")
