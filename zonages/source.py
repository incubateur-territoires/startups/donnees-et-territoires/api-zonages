from typing import NamedTuple, Optional


class Source(NamedTuple):
    nom: str
    url: str
    licence: Optional[str] = None
