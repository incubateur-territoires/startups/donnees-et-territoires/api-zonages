from .source import Source
from .zonage import Zonage


class QPV(Zonage):
    """
    Quartiers prioritaires de la politique de la ville (QPV)
    """

    SOURCE = Source(
        nom="ANCT",
        url="https://www.data.gouv.fr/fr/datasets/quartiers-prioritaires-de-la-politique-de-la-ville-qpv/",  # noqa
        licence="Licence Ouverte",
    )

    SQL_QUERY = """
        SELECT nom_qp, code_qp, commune_qp FROM qpv
            WHERE ST_Contains(geom, $1) LIMIT 1
        """

    def format_row(self, row: dict) -> dict:
        row = {
            "code": row.get("code_qp"),
            "nom": row.get("nom_qp"),
            "commune": row.get("commune_qp"),
        }
        return super().format_row(row)
