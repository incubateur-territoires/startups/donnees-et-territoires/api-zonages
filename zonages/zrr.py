from .source import Source
from .zonage import Zonage


class ZRR(Zonage):
    """
    Zone de revitalisation rurale
    """

    SOURCE = Source(
        nom="ANCT",
        url="https://www.data.gouv.fr/fr/datasets/zones-de-revitalisation-rurale-zrr/",
        licence="Licence Ouverte",
    )

    # On recherche dans quelle commune de 2017 étaient les coordonnées,
    # ce qui nous permet de vérifier le classement en ZRR ou non.
    SQL_QUERY = """
        SELECT insee_com, lib_com, classement, detail FROM zrr
            JOIN communes_2017 ON zrr.insee_com = communes_2017.insee
            WHERE ST_Contains(communes_2017.geom, $1) LIMIT 1
        """

    def format_row(self, row: dict) -> dict:
        row = {
            "commune": {
                "code_insee": row.get("insee_com"),
                "nom": row.get("lib_com"),
                "cog": 2017,
            },
            "classement": self._expand(row.get("classement"), self.CLASSEMENT),
            "detail": self._expand(row.get("detail"), self.DETAIL),
        }
        return super().format_row(row)

    def _expand(self, code, descriptions):
        return {
            "code": code,
            "description": descriptions.get(code, code),
        }

    CLASSEMENT = {
        "C": "Classée en ZRR",
        "NC": "Non classée",
        "P": "Partiellement classée en ZRR",
    }

    DETAIL = {
        "A": "Sortante en 2017 continuant de bénéficier des effets du classement depuis 2018",  # noqa
        "C": "Classée en ZRR",
        "D": "Classée en ZRR au titre de la baisse de population depuis 40 ans",
        "M": "Sortante en 2017 classée en zone de montagne continuant de bénéficier des effets du classement depuis 2017",  # noqa
        "NC": "Non classée",
        "P": "Partiellement classée en ZRR",
    }
